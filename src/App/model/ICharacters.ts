import { IModelGeneric } from "./IGeneric";



export interface IModelThumbnail {
  path: string;
  extension: string;
}

export interface IModelUrl {
  type: string;
  url: string;
}

export interface IModelResult {
  id: number;
  name: string;
  description: string;
  modified: Date;
  resourceURI: string;
  url: IModelUrl[];
  thumbnail: IModelThumbnail;
  comics: IModelGeneric;
  stories: IModelGeneric;
  events: IModelGeneric;
  series: IModelGeneric;
}

export interface IModelData {
  offset: number;
  limit: number;
  total: number;
  count: number;
  result: IModelResult[];
}

export interface IModelCharacter {
  code: number;
  status: string;
  copyright: string;
  attributionText: string;
  attributionHTML: string;
  data: IModelData;
  etag: string;
}

export interface IModelItems {
    resourceURI?: string;
    name?: string;
    type?: string;
    role? : string;
  }
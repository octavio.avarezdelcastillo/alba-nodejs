
export interface IEntityComic{
    last_sync: Date;
    name : string;
    pencilers: string[];
    writer: string[];
    inker: string[];
    letterer: string[];
    colorist: string[];
    editor: string[];
}
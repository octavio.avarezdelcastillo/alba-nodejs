export interface IEntityCharacter {
  character: string;
  comics: string[];
}

export interface IEntityDetailCharacter{
  last_sync: Date;
  name: string;
  characters: IEntityCharacter[];
}

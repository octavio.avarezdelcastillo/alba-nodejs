import { IModelGeneric } from "./IGeneric";

export interface IModelObjectGeneric {
  resourceURI?: string;
  name?: string;
  path?: string;
  extension?: string;
  type?: string;
  date?: Date;
  price?: number;
}

export interface IModelUrlGeneric {
  type: string;
  url: string;
}

export interface IModelTextObjects {
  type: string;
  language: string;
  text: string;
}

export interface IModelResultComic {
  id: number;
  digitalId: number;
  title: string;
  issueNumber: number;
  variantDescription: string;
  description: string;
  modified: Date;
  isbn: string;
  upc: string;
  diamondCode: string;
  ean: string;
  issn: string;
  format: string;
  pageCount: number;
  textObjects: IModelTextObjects[];
  resourceURI: string;
  urls: IModelUrlGeneric[];
  series: IModelObjectGeneric;
  variants: IModelObjectGeneric[];
  collections: IModelObjectGeneric[];
  collectedIssues: IModelObjectGeneric[];
  dates: IModelObjectGeneric[];
  prices: IModelObjectGeneric[];
  thumbnail: IModelObjectGeneric[];
  images: IModelObjectGeneric[];
  creators: IModelGeneric;
  stories: IModelGeneric;
  events: IModelGeneric;
}

export interface IModelComicData {
  offset: number;
  limit: number;
  total: number;
  count: number;
  result: IModelResultComic[];
}

export interface IComic {
  code: number;
  status: string;
  copyright: string;
  attributionText: string;
  attributionHTML: string;
  data: IModelComicData;
}

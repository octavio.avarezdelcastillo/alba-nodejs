import { IModelItems } from "./IItems";

export interface IModelGeneric {
    available: number;
    returned: number;
    collectionURI: string;
    items: IModelItems[];
  }
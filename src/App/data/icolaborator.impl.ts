import { model, Model, Schema } from "mongoose";
import IModelColaborator from "./imodel.colaborator";

class SchemaCharacter extends Schema {
  constructor() {
    super({
      last_sync: { type: Date, default: null },
      name: { type: String },
      pencilers: [{ type: String }],
      writer: [{ type: String }],
      inker: [{ type: String }],
      letterer: [{ type: String }],
      colorist: [{ type: String }],
      editor: [{ type: String }],
    },
    {
      bufferCommands: true,
      autoCreate: true 
    });
  }
}

const schemaCharacter = new SchemaCharacter();

export const ModelColaborator: Model<IModelColaborator> = model<IModelColaborator>(
  "Colaborator",
  schemaCharacter
);

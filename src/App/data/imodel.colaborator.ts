import { Document } from "mongoose";

export default interface IModelColaborator extends Document {
  last_sync: Date;
  name : string;
  pencilers: string[];
  writer: string[];
  inker: string[];
  letterer: string[];
  colorist: string[];
  editor: string[];
}

import { model, Model, Schema } from "mongoose";
import IModelCharacter from "./imodel.comics";

class SchemaCharacter extends Schema {
  constructor() {
    super(
      {
        last_sync: { type: Date, default: null },
        name: { type: String },
        characters: [
          {
            character: { type: String },
            comics: [{ type: String }],
          },
        ],
      },
      {
        bufferCommands: true,
        autoCreate: true,
      }
    );
  }
}

const schemaCharacter = new SchemaCharacter();

export const ModelCharacter: Model<IModelCharacter> = model<IModelCharacter>(
  "Character",
  schemaCharacter
);

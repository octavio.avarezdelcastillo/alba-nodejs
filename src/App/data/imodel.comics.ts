import { Document, Types } from "mongoose";

export default interface IModelCharacters extends Document {
  last_sync: Date;
  name: string;
  characters: IModelCharacter[];
}

export interface IModelCharacter extends Types.Subdocument {
  character: string;
  comics: string[];
}

import { MarvelRepositoryImpl, MarvelRepositoryCharactersImp} from "../infra";
import * as express from "express";
import { inject, injectable } from "inversify";
import TYPES from "../../types";

import {
  BaseHttpController,
  controller,
  httpGet,
  request,
  requestParam,
  response,
} from "inversify-express-utils";
import { IModelCharacter } from "../model/ICharacters";
import { JsonResult } from "inversify-express-utils/lib/results/JsonResult";
import { IEntityComic, IEntityDetailCharacter } from "../model";

@controller("/marvel")
export default class MarvelController extends BaseHttpController {
  private marvelService: MarvelRepositoryImpl;
  private marvelCharacters: MarvelRepositoryCharactersImp;

  constructor(
    @inject(TYPES.MarvelService) marvelService: MarvelRepositoryImpl,
    @inject(TYPES.MarvelCharacters) marvelCharacters: MarvelRepositoryCharactersImp
  ) {
    super();
    this.marvelService = marvelService;
    this.marvelCharacters = marvelCharacters;
  }

  @httpGet("/")
  public async index(
    @request() req: express.Request,
    @response() res: express.Response
  ) {
    try {
      res.status(200).send();
    } catch (error) {
      res.status(400).json(error);
    }
  }

  @httpGet("/colaborators/:name")
  public async getCharacterInterective(
    @requestParam("name") name: string,
    @response() res: express.Response
  ): Promise<IEntityComic | JsonResult> {
    try {
      return await this.marvelService.getCharacteres(name);
    } catch (error) {
      res.status(400).json(error);
    }
  }

  @httpGet("/characters/:name")
  public async getColaborators(
    @requestParam("name") name: string,
    @response() res: express.Response
  ): Promise<IEntityDetailCharacter | JsonResult> {
    try {
      return await this.marvelCharacters.getCharacterInterective(name);
    } catch (error) {
      res.status(400).json(error);
    }
  }
}

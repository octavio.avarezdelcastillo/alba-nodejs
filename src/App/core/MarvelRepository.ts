import { IEntityComic } from "../model/IEntityComic";

export interface MarvelRepository {
  getCharacteres(name: string): Promise<IEntityComic>;
}

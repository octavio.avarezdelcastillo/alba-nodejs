import { IEntityDetailCharacter } from "../model/ICharacterIinter";

export interface MarvelRepositoryCharacters {
  getCharacterInterective(name: string): Promise<IEntityDetailCharacter>;
}

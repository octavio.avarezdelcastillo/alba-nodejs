import {
  IModelCharacter,
  IModelResult,
  IEntityComic,
  IEntityCharacter,
  IEntityDetailCharacter,
} from "../model";
import { injectable } from "inversify";
import { MarvelRepositoryCharacters } from "../core";
import { Md5 } from "md5-typescript";
import axios from "axios";
import * as R from "ramda";
import { PrincipalService } from ".";
import {ModelCharacter} from "../data/icharacter.impl"

@injectable()
export class MarvelRepositoryCharactersImp
  implements MarvelRepositoryCharacters
{
  async getCharacterInterective(name: string): Promise<IEntityDetailCharacter> {
    try {
      const idCharacter = new PrincipalService();
      const id = await idCharacter.getIdCharacter(name);
      const characters: IEntityDetailCharacter = await this.getCharacteres(
        id.toString(),
        name
      );
      console.log(characters);
        const findSuperHeroe = await ModelCharacter.findOne({ name: name });
      if (findSuperHeroe === null) {
        await ModelCharacter.create(characters);
      } else {
        await ModelCharacter.findOneAndUpdate({ name: name }, characters);
      }
      return characters;
    } catch (error) {
      console.log(error);
    }
  }

  async getCharacteres(
    id: string,
    name: string
  ): Promise<IEntityDetailCharacter> {
    try {
      const url = `https://gateway.marvel.com:443/v1/public/characters/${id}/stories`;
      const dateHash = new Date().getTime().toString();
      const { data } = await axios.get(url, {
        params: {
          ts: dateHash,
          apikey: process.env.API_KEY,
          hash: Md5.init(
            dateHash +
              "3f131f5315c6c99ffd92cf10d1d5fa6779e0ee68" +
              process.env.API_KEY
          ),
        },
        headers: {
          "Content-Type": "application/json",
        },
      });

      const { results } = data.data;
      const characters: string[] = [];
      results.forEach((element) => {
        element.characters.items.forEach((character) => {
          characters.push(character.name);
        });
      });

      let uniqueCharacter: string[] = characters.filter((c, index) => {
        return characters.indexOf(c) === index;
      });

      const response: IEntityCharacter[] = [];
      uniqueCharacter.forEach((comicCharater) => {
        const comics: string[] = [];
        results.forEach((element) => {
          element.characters.items.forEach((character) => {
            if (comicCharater === character.name) {
              comics.push(element.title);
            }
          });
        });
        const structure = {
          character: comicCharater,
          comics: comics,
        } as IEntityCharacter;
        response.push(structure);
      });

      return {
        last_sync: new Date(),
        name: name,
        characters: response,
      } as IEntityDetailCharacter;
    } catch (error) {
      console.log(error);
    }
  }
}

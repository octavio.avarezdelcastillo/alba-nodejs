import { IModelCharacter, IModelResult, IEntityComic } from "../model";
import { injectable } from "inversify";
import { MarvelRepository } from "../core/index";
import { Md5 } from "md5-typescript";
import { Utils } from "../../utils/utils";
import axios from "axios";
import * as R from "ramda";
import { ModelColaborator } from "../data/icolaborator.impl";
import { PrincipalService } from "./PrincipalService";

@injectable()
export class MarvelRepositoryImpl implements MarvelRepository {
  async getCharacteres(name: string): Promise<IEntityComic> {
    try {
      const idCharacter = new PrincipalService();
      const id = await idCharacter.getIdCharacter(name);
      const entityComic: IEntityComic = await this.getComics(
        id.toString(),
        name
      );
      const findSuperHeroe = await ModelColaborator.findOne({ name: name });
      if (findSuperHeroe === null) {
        await ModelColaborator.create(entityComic);
      } else {
        await ModelColaborator.findOneAndUpdate({ name: name }, entityComic);
      }
      return entityComic;
    } catch (error) {
      console.log(error);
    }
  }

  async getComics(id: string, name: string): Promise<IEntityComic> {
    try {
      const url = `https://gateway.marvel.com:443/v1/public/characters/${id}/comics`;
      const dateHash = new Date().getTime().toString();
      const { data } = await axios.get(url, {
        params: {
          ts: dateHash,
          apikey: process.env.API_KEY,
          hash: Md5.init(
            dateHash +
              "3f131f5315c6c99ffd92cf10d1d5fa6779e0ee68" +
              process.env.API_KEY
          ),
        },

        headers: {
          "Content-Type": "application/json",
        },
      });

      const { results } = data.data;

      const pencilers: string[] = [];
      const writer: string[] = [];
      const inker: string[] = [];
      const letterer: string[] = [];
      const colorist: string[] = [];
      const editor: string[] = [];

      results.forEach((resul) => {
        resul.creators.items.forEach((element) => {
          if (/^writer/.test(element.role)) {
            writer.push(element.name);
          }
          if (/^inker/.test(element.role)) {
            inker.push(element.name);
          }
          if (/^letterer/.test(element.role)) {
            letterer.push(element.name);
          }
          if (/^colorist/.test(element.role)) {
            colorist.push(element.name);
          }
          if (/^penciler/.test(element.role)) {
            pencilers.push(element.name);
          }
          if (/^editor/.test(element.role)) {
            editor.push(element.name);
          }
        });
      });

      const comicDetail = {
        last_sync: new Date(),
        name: name,
        pencilers: pencilers,
        writer: writer,
        inker: inker,
        letterer: letterer,
        colorist: colorist,
        editor: editor,
      } as IEntityComic;

      return comicDetail;
    } catch (error) {
      console.log(error);
    }
  }
}

import { IModelCharacter, IModelResult, IEntityComic } from "../model";
import axios from "axios";
import * as R from "ramda";
import { Md5 } from "md5-typescript";


export class PrincipalService{

    async getIdCharacter(name: string): Promise<number> {
        try {
          const url = "https://gateway.marvel.com:443/v1/public/characters";
          const dateHash = new Date().getTime().toString();
          const { data } = await axios.get(url, {
            params: {
              name: name,
              ts: dateHash,
              apikey: process.env.API_KEY,
              hash: Md5.init(
                dateHash +
                  "3f131f5315c6c99ffd92cf10d1d5fa6779e0ee68" +
                  process.env.API_KEY
              ),
            },
    
            headers: {
              "Content-Type": "application/json",
            },
          });
    
          const result = R.pipe(R.pathOr(null, ["data", "results"]))(
            data
          )[0] as IModelResult;
    
          return result.id;
        } catch (error) {
          console.log(error);
        }
      }
}
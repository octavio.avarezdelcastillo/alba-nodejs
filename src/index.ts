import App from "./app";
import logger from "./utils/logger";

function main () {
  try {
    const app = new App();
    app.nameApp();
    app.initConfig();
    app.build();
    app.listen();
    logger.info(`**************** S E R V E R **********************`);
  } catch (error) {
    console.log(error);
  }
};

main();
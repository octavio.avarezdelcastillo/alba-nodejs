import "reflect-metadata";
import logger from "./utils/logger";
import * as express from "express";
import { InversifyExpressServer } from "inversify-express-utils";
import container from "./inversify.config";
import morgan from "morgan";
import "./App/controller/indexHandler";
import asciitext from "ascii-art";
import { MongoDB } from "./conectionDb";

class App {
  public port?: number;
  private server: InversifyExpressServer;
  private app: express.Application;
  private data: MongoDB;

  constructor() {
    this.port = 8899;
    this.server = new InversifyExpressServer(container);
    this.data = new MongoDB();
    this.initConnectMongo();
  }

  public initConnectMongo() {
    this.data.initConnection("albo", "albo", "marvel_db", process.env.MONGO_SERVER, 27017);
    //  this.data.initConnection("albo", "albo", "marvel_db", "mongodb_server" ,27017);
     this.data.setAutoReconnect("albo", "albo", "marvel_db", process.env.MONGO_SERVER, 27017);
  }

  public initConfig() {
    this.server.setConfig((app: express.Application) => {
      app.use(express.json({ limit: "5mb" }));
      app.use(morgan("dev"));
    });
  }

  public build() {
    this.app = this.server.build();
  }

  public nameApp() {
    logger.info(asciitext.style("EXAMEN ALBO"));
  }

  public listen() {
    this.app.listen(this.port, () => {
      logger.info(
        `**************** Server port ${this.port} **********************`
      );
    });
  }

  public closeMongo(){
    this.data.disconnect();
    process.exit(0);
  }

}

export default App;

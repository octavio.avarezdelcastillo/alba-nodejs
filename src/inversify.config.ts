import TYPES from './types';

import { Container } from 'inversify';
import { interfaces , TYPE } from 'inversify-express-utils';
import { MarvelRepositoryImpl ,MarvelRepositoryCharactersImp } from './App/infra';


const container = new Container();

container.bind<MarvelRepositoryImpl>(TYPES.MarvelService).to(MarvelRepositoryImpl).inSingletonScope();
container.bind<MarvelRepositoryCharactersImp>(TYPES.MarvelCharacters).to(MarvelRepositoryCharactersImp).inSingletonScope();

export default container;
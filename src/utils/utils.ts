export class Utils {
  public static regexFind(reg: string, buff: string): boolean {
    const pattern = /reg/gi;
    return pattern.test(buff);
  }
}

import mongoose from "mongoose";
import { inject, injectable } from "inversify";
import logger from "./utils/logger";

export class MongoDB {
  public async initConnection(
    user: string,
    pass: string,
    dbName: string,
    connection: string,
    port: number
  ): Promise<typeof mongoose> {
    if (user && pass && dbName && connection) {
      return await mongoose
        .connect(
          `mongodb://${user}:${pass}@${connection}:${port}/${dbName}`
        )
        .then((conn) => {
          logger.info(
            `the database connection to mongodb://${user}:${pass}@${connection}:${port}/${dbName} has been successfully`
          );
          return conn;
        })
        .catch((err) => {
          logger.error(err);

          logger.error(
            `Error connecting with database mongodb://${user}:${pass}@${connection}:${port}/${dbName}`
          );
          return process.exit(1);
        });
    } else {
      return await mongoose
        .connect(`mongodb://${user}:${pass}@${connection}:${port}/${dbName}`)
        .then((conn) => {
          logger.info(
            `the database connection to ${connection} has been successfully`
          );
          return conn;
        })
        .catch(() => {
          logger.error(`Error connecting with database ${connection}`);
          return process.exit(1);
        });
    }
  }

  public setAutoReconnect(
    user: string,
    pass: string,
    dbName: string,
    connection: string,
    port: number
  ) {
    mongoose.connection.on("disconnected", () => {
      this.initConnection(user, pass, dbName, connection, port);
    });
  }

  public setEvents(url: string) {
    mongoose.connection.on("connected", () => {
      logger.info(`Mongoose default connection is open to ${url}`);
    });

    mongoose.connection.on("error", (err) => {
      logger.info("Mongoose default connection has occured " + err + " error");
    });

    mongoose.connection.on("disconnected", () => {
      logger.info("Mongoose default connection is disconnected");
    });

    process.on("SIGINT", () => {
      mongoose.connection.close(() => {
        logger.info(
          "Mongoose default connection is disconnected due to application termination"
        );
        process.exit(0);
      });
    });
  }

  public async disconnect() {
    await mongoose.connection.close();
  }
}

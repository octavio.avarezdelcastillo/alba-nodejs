import App from "../app";
import logger from "../utils/logger";
import { expect } from "chai";
import "mocha";
import chai from "chai";
import chaiHttp from "chai-http";
import supertest from "supertest";
let app ;


chai.use(chaiHttp);

before(async () => {
  app=new App();
  app.nameApp();
  app.initConfig();
  app.build();
  app.listen();
  logger.info(`**************** S E R V E R - T E S T **********************`);
});

describe("Health srever test", () => {
  it("should return 200", () => {
     chai
      .request("http://localhost:8899")
      .get("/marvel/")
      .then((res) => {
        console.log(res.status);
      });
  });
});



describe("Marvel request get", () => {
  it("should return the list ", () => {
    const superHereo = 'Captain America';
     chai
      .request("http://localhost:8899")
      .get("/marvel/colaborators/Captain America")
      .then((res) => {
        console.log(res.status);
      });
  });
});


after(async ()=>{
  app.closeMongo();
});
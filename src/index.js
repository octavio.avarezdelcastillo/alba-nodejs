"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = require("./app");
const logger_1 = require("./utils/logger");
void (async () => {
    try {
        console.log("HOLA");
        const app = new app_1.default();
        app.initConfig();
        app.build();
        app.listen();
        logger_1.default.info(`**************** S E R V E R **********************`);
    }
    catch (error) {
        console.log(error);
    }
});
//# sourceMappingURL=index.js.map
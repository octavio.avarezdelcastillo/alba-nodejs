import App from "./app";
import logger from "./utils/logger";

let app = new App();
app.nameApp();
app.initConfig();
app.build();
app.listen();
logger.info(`**************** S E R V E R - T E S T **********************`);

export default app = app;
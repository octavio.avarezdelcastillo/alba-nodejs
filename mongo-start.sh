#!/bin/bash

 mongod --fork --logpath /var/log/mongodb/mongod.log

 mongo init-mongo.js

 mongod --shutdown 

 mongod --bind_ip=0.0.0.0 

FROM node:16-stretch

LABEL Octavio Esquivel Alvarez del Castillo <octavio.avarezdelcastillo@gmail.com>

WORKDIR /app

COPY src /app
COPY package.json /app
COPY tsconfig.json /app

RUN apt update && apt install jq original-awk -y
RUN npm install pm2 && npm install --global yarn cross-env --force 
RUN npm install --save concurrently
RUN chown -R node:node /app
ENV NPM_CONFIG_LOGLEVEL warn
EXPOSE 8899

USER node